import 'package:flutter/cupertino.dart';

class Constant {
  static const BoxDecoration gradientAppbar = BoxDecoration(
      gradient: LinearGradient(
    stops: [
      0.10,
      0.90,
    ],
    begin: Alignment.bottomLeft,
    end: Alignment.topRight,
    colors: [
      Color(0xFF74EAF1),
      Color(0xFF0CA7A7),
    ],
  ));
}
